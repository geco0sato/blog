@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-lg-3">

    <div class="card">
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          <div><strong>{{ $user->display_name }}</strong></div>
          <div class="text-muted">&#064;{{ $user->name }}</div>
          @if(Auth::check() && Auth::id() == $user->id)
          <div class="mt-3">
            <a class="small text-muted" href="{{ route('users.edit', $user->name) }}"><i class="fas fa-user-cog"></i> 編集</a>
          </div>
          @endif
        </li>
      </ul>
    </div>

  </div>

  <div class="col-lg-9 mt-sm-3">

    <div class="card">
      <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" href="#">最近の記事 <span class="badge badge-success">{{ $posts->total() }}</span></a>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <ul class="list-group list-group-flush">
        @foreach($posts as $post)
          <li class="list-group-item">
            <h5 class="card-title">
              <a class="text-dark font-weight-bold" href="{{ route('posts.show', [$post->user->name, $post->id]) }}">{{ $post->title }}</a>
            </h5>
            <span class="card-text text-muted small">by {{ $user->name }}</span>
            <span class="card-text text-muted small ml-2 mr-2">{{ $post->updated_at->diffForHumans() }}</span>
            @if(Auth::check() && Auth::id() == $user->id)
              @if($post->is_publish)
                <span class="badge badge-success">公開中</span>
              @else
                <span class="badge badge-secondary">非公開</span>
              @endif
            @endif
          </li>
        @endforeach
        </ul>
        <div class="mt-4">
          {{ $posts->links() }}
        </div>
      </div>
    </div>
  </div>

</div>

@endsection
