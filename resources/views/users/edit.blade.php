@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <h5 class="card-header">ユーザー情報編集</h5>

      <div class="card-body">

        <form method="POST" action="{{ route('users.update', $user->name) }}">
          @csrf
          @method('PUT')

          <div class="form-group">
            <label for="user_display_name">表示名</label>
            <input type="text" class="form-control @error('display_name') is-invalid @enderror" id="user_display_name" name="display_name" value="{{ old('display_name', $user->display_name) }}" required placeholder="表示名を入力してください">
            @error('name')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label for="user_name">ユーザー名</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="user_name" name="name" value="{{ old('name', $user->name) }}" required placeholder="ユーザー前を入力してください">
            @error('name')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label for="user_email">メールアドレス</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="user_email" name="email" value="{{ old('email', $user->email) }}" required autocomplete="email" placeholder="メールアドレスを入力してください">
            @error('email')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>

          <button type="submit" class="btn btn-success">編集</button>
        </form>
      </div>

    </div>
  </div>
</div>

@endsection
