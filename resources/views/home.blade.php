@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-12">

    @if(count($posts))

      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs card-header-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" href="#">すべて <span class="badge badge-success">{{ $posts->total() }}</span></a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <ul class="list-group list-group-flush">
          @foreach($posts as $post)
            <li class="list-group-item">
              <h5 class="card-title">
                <a class="text-dark font-weight-bold" href="{{ route('posts.show', [$post->user->name, $post->id]) }}">{{ $post->title }}</a>
              </h5>
              <span class="card-text text-muted small">&#064;<a class="text-muted" href="{{ route('users.show', $post->user->name) }}">{{ $post->user->name }}</a></span>
              <span class="card-text text-muted small ml-2">{{ $post->created_at->format('Y年m月d日') }}</span>
              <span class="card-text text-muted small ml-2">{{ $post->updated_at->diffForHumans() }}</span>
            </li>
          @endforeach
          </ul>
          <div class="mt-4">
            {{ $posts->links() }}
          </div>
        </div>
      </div>

    @endif

  </div>
</div>

@endsection
