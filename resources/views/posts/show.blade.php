@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12">

    <div class="card">
      <div class="card-body">
        <div class="mb-4">
          <span class="card-text text-muted">&#064;<a class="text-muted" href="{{ route('users.show', $post->user->name) }}">{{ $user->name }}</a></span>
          <span class="card-text text-muted ml-2 mr-2">{{ $post->updated_at->format('Y年m月d日') }}に更新</span>
          @if(Auth::check())
            <div class="float-right">
              @if(Auth::id() == $post->user_id)
                <a class="small text-muted" href="{{ route('posts.edit', $post->id) }}"><i class="far fa-edit"></i> 編集</a>
              @endif
            </div>
          @endif
        </div>
        <h1 class="card-title"><strong>{{ $post->title }}</strong></h1>
        <div class="mb-5">
          @if(! $post->is_publish)
            <span class="badge badge-secondary">非公開</span>
          @endif
        </div>
        <div class="markdown-body">
          {!! $post->body !!}
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
