@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <h5 class="card-header">記事編集</h5>

      <div class="card-body">

        <form method="POST" action="{{ route('posts.update', $post->id) }}">
          @csrf
          @method('PUT')

          <div class="form-group">
            <div class="custom-control custom-switch">
              <input type="checkbox" class="custom-control-input @if($errors->has('is_publish')) is-invalid @endif" id="post_is_publish" name="is_publish" value="1" @if(old('is_publish', $post->is_publish)) checked @endif>
              <label class="custom-control-label" for="post_is_publish">公開する</label>
            </div>
            @foreach($errors->get('is_publish') as $message)
              <div class="invalid-feedback" style="display:block;">{{ $message }}</div>
            @endforeach
          </div>


          <div class="form-group">
            <label for="post_title">タイトル</label>
            <input type="text" class="form-control @if($errors->has('title')) is-invalid @endif" id="post_title" name="title" value="{{ old('title', $post->title) }}" placeholder="タイトルを入力してください">
            @foreach($errors->get('title') as $message)
              <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
          </div>

          <div class="form-group">
            <label for="post_body">本文</label>
            <textarea class="form-control @if($errors->has('body')) is-invalid @endif" id="post_body" name="body" rows="12" placeholder="学んだことをMarkdown記法で入力してください">{{ old('body', $post->body) }}</textarea>
            @foreach($errors->get('body') as $message)
              <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
          </div>

          <button type="submit" class="btn btn-success">編集</button>
          <div class="float-right">
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">削除</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">本当に削除しますか？</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
        <form method="POST" action="{{ route('posts.destroy', $post->id) }}">
          @csrf
          @method('DELETE')
          <button type="submit" class="btn btn-danger">削除</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
