@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <h5 class="card-header">記事一覧</h5>

      <div class="card-body">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">タイトル</th>
              <th scope="col">公開</th>
              <th scope="col">更新日時</th>
              <th scope="col">作成日時</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($posts as $post)
              <tr>
                <td scope="row">{{ $post->title }}</td>
                <td>
                  @if($post->is_publish)
                    <span class="badge badge-success">公開</span>
                  @else
                    <span class="badge badge-secondary">非公開</span>
                  @endif
                </td>
                <td>{{ $post->updated_at }}</td>
                <td>{{ $post->created_at }}</td>
                <td>
                  <a class="btn btn-sm btn-primary" href="{{ route('posts.edit', $post->id) }}" role="button">編集</a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {{ $posts->links() }}
      </div>

    </div>
  </div>
</div>

@endsection
