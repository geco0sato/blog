@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <h5 class="card-header text-white bg-dark">新規記事</h5>

      <div class="card-body">

        <form method="POST" action="{{ route('posts.store') }}">
          @csrf

          <div class="form-group">
            <div class="custom-control custom-switch">
              <input type="checkbox" class="custom-control-input @if($errors->has('is_publish')) is-invalid @endif" id="post_is_publish" name="is_publish" value="1" @if(old('is_publish')) checked @endif>
              <label class="custom-control-label" for="post_is_publish">公開する</label>
            </div>
            @foreach($errors->get('is_publish') as $message)
              <div class="invalid-feedback" style="display:block;">{{ $message }}</div>
            @endforeach
          </div>


          <div class="form-group">
            <label for="post_title">タイトル</label>
            <input type="text" class="form-control @if($errors->has('title')) is-invalid @endif" id="post_title" name="title" value="{{ old('title') }}" placeholder="タイトルを入力してください">
            @foreach($errors->get('title') as $message)
              <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
          </div>

          <div class="form-group">
            <label for="post_body">本文</label>
            <textarea class="form-control @if($errors->has('body')) is-invalid @endif" id="post_body" name="body" rows="12" placeholder="学んだことをMarkdown記法で入力してください">{{ old('body') }}</textarea>
            @foreach($errors->get('body') as $message)
              <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
          </div>

          <button type="submit" class="btn btn-success">作成</button>
        </form>
      </div>

    </div>
  </div>
</div>

@endsection
