<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Qiiji</title>

    <!-- Bootstrap core CSS -->
    <!-- Custom styles for this template -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

  </head>
  <body>
    <nav class="navbar navbar-expand navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="{{ route('home') }}">Qiiji</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav d-none d-md-block mr-3">
          <li class="nav-item @if(Route::currentRouteName() == 'home') active @endif">
            <a class="nav-link" href="{{ route('home') }}">ホーム</a>
          </li>
        </ul>
        <form class="form-inline d-none">
          <input class="form-control form-control-sm col-9 col-sm-auto" type="text" placeholder="キーワード入力" aria-label="Search">
          <button class="btn btn-sm btn-secondary ml-1" type="submit">検索</button>
        </form>

        @if(Auth::user())
          <div class="ml-auto">
            <a class="btn btn-sm btn-warning d-none d-sm-block" href="{{ route('posts.create') }}" role="button"><i class="far fa-edit"></i> 新規記事</a>
          </div>
          <ul class="navbar-nav ml-2">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i> {{ Auth::user()->name }}</a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('users.show', Auth::user()->name) }}">マイページ</a>
                <div class="dropdown-divider"></div>
                <form method="POST" action="{{ route('logout') }}">
                  @csrf
                  <button type="submit" class="dropdown-item"><i class="fas fa-sign-out-alt"></i> ログアウト</button>
                </form>
              </div>
            </li>
          </ul>
        @else
          <div class="ml-auto">
            <a class="btn btn-sm btn-outline-light" href="{{ route('register') }}" role="button">新規登録</a>
            <a class="btn btn-sm btn-light" href="{{ route('login') }}" role="button">ログイン</a>
          </div>
        @endif

      </div>
    </nav>

    <div class="container-fluid">
      <main role="main">
      
        @yield('content')
      
      </main>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
  </body>
</html>
