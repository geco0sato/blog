@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
  <div class="col-lg-8">
    <div class="card">

      <div class="card-body">
        <h5 class="card-title mb-4">Qiijiにログイン</h5>
        <form method="POST" action="{{ route('login') }}">
          @csrf

          <div class="form-group">
            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="ユーザー名 または メールアドレス">
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="current-password" placeholder="パスワード">
            @if (Route::has('password.request'))
              <a class="btn btn-sm btn-link" href="{{ route('password.request') }}">パスワードを忘れた場合</a>
            @endif
            @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label class="form-check-label" for="remember">自動ログイン</label>
            </div>
          </div>

          <button type="submit" class="btn btn-success">ログイン</button>

        </form>

      </div>
    </div>
  </div>
</div>

@endsection
