@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
  <div class="col-lg-8">
    <div class="card">

      <div class="card-body">
        <h5 class="card-title mb-4">パスワード再設定メールを送る</h5>

        @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
        @csrf

          <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="メールアドレス">
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <button type="submit" class="btn btn-primary">送信</button>
        </form>

      </div>
    </div>
  </div>
</div>

@endsection
