@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
  <div class="col-lg-8">
    <div class="card">

      <div class="card-body">
        <h5 class="card-title">Qiijiへようこそ！</h5>
        <p class="card-text mb-4">新規登録(無料)して利用を開始しましょう。</p>
        <form method="POST" action="{{ route('register') }}">
        @csrf

          <div class="form-group">
            <input type="text" class="form-control @error('display_name') is-invalid @enderror" id="display_name" name="display_name" value="{{ old('display_name') }}" required autocomplete="display_name" autofocus placeholder="表示名">
            @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="ユーザ名(英数字)">
            @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="メールアドレス">
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" value="" required autocomplete="new-password" placeholder="パスワード">
            @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <input type="password" class="form-control" id="password-confirm" name="password_confirmation" value="" required autocomplete="new-password" placeholder="パスワード(確認用)">
          </div>

          <button type="submit" class="btn btn-success mt-3">新規登録</button>

        </form>

      </div>
    </div>
  </div>
</div>

@endsection
