<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('{user_name}', 'UserController@show')->name('users.show');
Route::get('{user_name}/edit', 'UserController@edit')->name('users.edit');
Route::put('{user_name}', 'UserController@update')->name('users.update');

Route::get('{user_name}/posts/{post}', 'PostController@show')->name('posts.show');
Route::resource('posts', 'PostController')->except(['index', 'show']);
