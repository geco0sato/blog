<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'bail|required|string|max:16',
            'name' => [
                'bail',
                'required',
                'string',
                'max:32',
                'unique:users,name,'. Auth::id(). ',id',
                function ($attribute, $value, $fail) {
                    if (! preg_match('/^[A-Za-z\d]+$/', $value)) {
                        $fail('ユーザ名はアルファベット数字がご利用できます。');
                    }
                },
            ],
            'email'        => 'bail|required|email|max:255|unique:users,name,'. Auth::id(). ',id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'display_name' => '表示名',
            'name'         => 'ユーザー名',
            'email'        => 'メールアドレス',
        ];
    }
}
