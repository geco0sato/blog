<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Requests\UpdateUser;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $userName
     * @return \Illuminate\Http\Response
     */
    public function show(string $userName)
    {
        $user = User::where('name', $userName)->first();

        $postSelect = $user->posts();
        if(! Auth::check() || Auth::id() != $user->id) {
            $postSelect->where('is_publish', true);
        }
        $posts = $postSelect->orderBy('id', 'DESC')->paginate();

        $viewData = [
            'user'  => $user,
            'posts' => $posts,
        ];

        return view('users.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $userName
     * @return \Illuminate\Http\Response
     */
    public function edit(string $userName)
    {
        $user = User::where('name', $userName)->first();

        $viewData = [
            'user' => $user,
        ];

        return view('users.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UpdateUser  $request
     * @param  string  $userName
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, String $userName)
    {
        $user = User::where('name', $userName)->first();

        $user->update($request->validated());

        return redirect()->route('users.show', $user->name);
    }
}
