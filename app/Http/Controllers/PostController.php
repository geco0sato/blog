<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Parsedown;
use App\Models\User;
use App\Models\Post;
use App\Http\Requests\StorePost;
use App\Http\Requests\UpdatePost;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $viewData = [
        ];

        return view('posts.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StorePost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $post = new Post();

        $post->fill($request->validated());
        $post->is_publish = $request->is_publish ? true : false;

        Auth::user()->posts()->save($post);

        return redirect()->route('users.show', Auth::user()->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $userName
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(string $userName, Post $post)
    {
        if(! $post->is_publish && Auth::id() != $post->user_id) {
            abort('404', 'ページが見つかりません。');
        }

        $parsedown = new Parsedown();
        $parsedown->setSafeMode(true);

        $post->body = $parsedown->text($post->body);

        $user = User::where('name', $userName)->first();

        $viewData = [
            'user' => $user,
            'post' => $post,
        ];

        return view('posts.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $viewData = [
            'user' => Auth::user(),
            'post' => $post,
        ];

        return view('posts.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UpdatePost  $request
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePost $request, Post $post)
    {

        $post->is_publish = $request->is_publish ? true : false;
        $post->update($request->validated());

        return redirect()->route('posts.show', [$post->user->name, $post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('users.show', Auth::user()->name);
    }
}
