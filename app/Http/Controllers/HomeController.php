<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::where('is_publish', true)->orderBy('id', 'DESC')->paginate();

        $viewData = [
            'posts' => $posts,
        ];

        return view('home', $viewData);
    }
}
