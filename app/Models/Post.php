<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['is_publish'];

    /**
     * Get the user that owns the posts.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
